export default {
  props: {
    autoLoginRedirect: {
      type: Boolean,
      default: true
    },
    loginUrl: {
      type: String,
      default: '/api/v1/auth/login/'
    },
    alreadyLoggedIn: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      logginIn: false,
      loggedIn: this.alreadyLoggedIn
    }
  },
  methods: {
    performLogin (ev) {
      this.logginIn = true
      let loginUrl = this.loginUrl + '?next=' + window.location.href

      if (this.autoLoginRedirect) {
        window.location.href = loginUrl
      } else {
        this.$emit('askPerformLogin', ev)
      }
      this.logginIn = false
    }
  },
  watch: {
    alreadyLoggedIn (n) {
      this.loggedIn = n
    }
  }
}
