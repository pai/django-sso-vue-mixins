export default {
  props: {
    logoutRedirects: {
      type: Boolean,
      default: false
    },
    logoutUrl: {
      type: String,
      default: '/api/v1/auth/logout/'
    },
    alreadyLoggedOut: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      logginOut: false,
      loggedOut: this.alreadyLoggedOut
    }
  },
  methods: {
    performLogout (ev) {
      this.logginOut = true
      let logoutUrl = this.logoutUrl + '?next=' + window.location.href

      if (this.logoutRedirects) {
        window.location.href = logoutUrl
      } else {
        this.$http.post(
          logoutUrl,
          {},
          {
            withCredentials: true
          }
        ).then((res) => {
          let redirectTo = '/'
          if (res.data && res.data.passepartout_redirect_url) {
            redirectTo = res.data.passepartout_redirect_url
          }

          this.logginOut = false

          window.location.replace(redirectTo)
        }).catch(() => {
          this.logginOut = false

          throw 'Error loggin out.'
        })
      }
    }
  },
  watch: {
    alreadyLoggedOut (n) {
      this.loggedOut = n
    }
  }
}
