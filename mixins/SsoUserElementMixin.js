/**
 * @mixin
 */

import LoadingElementMixin from '@paiuolo/pai-vue-mixins/mixins/LoadingElementMixin'

import LoginMixin from './LoginMixin'
import FetchUserProfileMixin from './FetchUserProfileMixin'
import UserProfileMixin from './UserProfileMixin'


export default {
  mixins: [
    LoadingElementMixin,
    LoginMixin,
    FetchUserProfileMixin,
    UserProfileMixin
  ],
  props: {
    userProfileUrl: {
      type: String,
      default: '/api/v1/auth/user/'
    },
    loginUrl: {
      type: String,
      default: '/login/'
    }
  },
  mounted () {
    this.$on('performingFetchUserProfile', () => {
//       console.log('performingFetchUserProfile')
      this.startLoading()
    })
    this.$on('userProfileFetched', () => {
//       console.log('userProfileFetched')
      this.stopLoading()
    })
    this.$on('userIsAnonymous', () => {
//       console.log('userIsAnonymous')
      this.stopLoading()
    })
    this.$on('errorFetchingUserProfile', () => {
//       console.log('errorFetchingUserProfile')
      this.stopLoading()
    })
  }
}
