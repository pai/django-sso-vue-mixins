/**
 * @mixin
 */

export default {
  props: {
    userProfileUrl: {
      type: String,
      default: '/api/v1/auth/user/'
    }
  },
  data () {
    return {
      fetchUserProfileMaxRetries: 0,
      fetchUserProfileRetries: 0,
      fetchUserProfileParams: {},
      fetchUserProfileOptions: {
        withCredentials: true
      },
      fetchUserProfilePermittedErrors: '401,403',
      fetchUserProfileRetryTimeout: 199,
      fetchUserProfileRetryOnPermittedError: false,
      userProfile: null
    }
  },
  methods: {
    fetchUserProfileError (error) {
      let raises = (error.response && error.response.status && this.fetchUserProfilePermittedErrors.indexOf(error.response.status) === -1)
      let retries = (raises || this.fetchUserProfileRetryOnPermittedError)

      if (error.response && this.fetchUserProfilePermittedErrors.indexOf(error.response.status) > -1) {
        this.$emit('userIsAnonymous')
        return
      }

      if (retries && (this.fetchUserProfileRetries < this.fetchUserProfileMaxRetries)) {
        this.fetchUserProfileRetries += 1

        setTimeout(() => {
          this.fetchUserProfile()
        }, this.fetchUserProfileRetryTimeout)
      } else {
        if (raises) {
          this.$emit('errorFetchingUserProfile', error)
          throw 'Error performing fetchUserProfile request'
        }
      } 
    },
    fetchUserProfile () {
      if (this.fetchUserProfileRetries === 0) {
        this.$emit('performingFetchUserProfile', this.name)
      }

      this.$http.get(
        this.userProfileUrl,
        this.fetchUserProfileOptions,
        this.fetchUserProfileParams
      ).then((response) => {
        this.userProfile = response.data
        this.$emit('userProfileFetched', response.data)
      }).catch((error) => {
        this.fetchUserProfileError(error)
      })
    }
  }
}
