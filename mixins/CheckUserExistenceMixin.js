import CredentialsTypeMixin from '@paiuolo/pai-vue-mixins/mixins/CredentialsTypeMixin'
import ErrorResponseMixin from '@paiuolo/pai-vue-mixins/mixins/ErrorResponseMixin'

let gettext = window.gettext || function (x) { return x }

export default {
  mixins: [
    CredentialsTypeMixin,
    ErrorResponseMixin
  ],
  props: {
    userCredentialsAsEmailOnly: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      userExistenceUrl: '/api/v1/profiles/users/check/'
    }
  },
  methods: {
    checkUserExistence (userCredentials) {
      let params = {}
      let credentialsError = null
      let loginType = this.getCredentialsType(userCredentials)

      if (this.credentialsTypeIsValid(userCredentials)) {
        if (loginType === 'email') {
          params['email'] = userCredentials
        } else {
          if (this.userCredentialsAsEmailOnly) {
            credentialsError = gettext('Insert your e-mail.')
          } else {
            params['username'] = userCredentials
          }
        }
      } else {
        credentialsError = gettext('Invalid credentials.')
      }
      if (credentialsError) {
        this.$emit('checkUserExistenceError', credentialsError)
      } else {
        this.$http.get(
          this.userExistenceUrl,
          {
            params: params
          }
        ).then((response) => {
          this.$emit('userExists', response)
        }).catch((error) => {
          if (error.response) {
            let status = error.response.status
            if (status === 404) {
              this.$emit('userDoesNotExists')
            } else {
              let parsedError = this.parseError(error)
              this.$emit('checkUserExistenceError', parsedError)
            }
          } else {
            let parsedError = this.parseError(error)
            this.$emit('checkUserExistenceError', parsedError)
          }
        })
      }
    }
  }
}
